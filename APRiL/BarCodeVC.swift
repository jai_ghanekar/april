//
//  BarCodeVC.swift
//  APRiL
//
//  Created by Jai Ghanekar on 5/17/16.
//  Copyright © 2016 Jai Ghanekar. All rights reserved.
//

import UIKit


class BarCodeVC: UIViewController {
    @IBOutlet weak var barcodeField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    override func viewDidLoad() {
       
        self.searchButton.backgroundColor = UTD_GREEN
        self.searchButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.searchButton.layer.cornerRadius = 5.0
        self.searchButton.layer.borderWidth = 1.0
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.topItem?.title = "Search for a Chemical"
        self.searchButton.setTitle("Search", forState: UIControlState.Normal)

    }
    
    
    @IBAction func searchAction(sender: AnyObject) {
        //later add check if valid barcode
        self.barcodeField.endEditing(true)
        self.searchButton.setTitle("Loading", forState: UIControlState.Normal)
        if let text = self.barcodeField.text {
            APRiLRequests.getChemInfoWithId(text, onSuccess: { (chemical) in
                print(chemical.name!)
                let chemInfoVC: ChemInfoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Info") as! ChemInfoVC
                chemInfoVC.chemical = chemical
                self.navigationController?.pushViewController(chemInfoVC, animated:true)
                
            }) { response in
                print(response)
            }
        }
        
     
        
        
        
    }
    
  
    
    

}
