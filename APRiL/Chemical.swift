//
//  Chemical.swift
//  APRiL
//
//  Created by Jai Ghanekar on 5/18/16.
//  Copyright © 2016 Jai Ghanekar. All rights reserved.
//

import Foundation
import SwiftyJSON
class Chemical {
    //look into optionals
    var fireCodes: [String]? = nil
    var nfpa: [String]? = nil
    var name: [String]? = nil
    var toxicology: [String]? = nil
    var regulations: [String]? = nil
    var conditions: [String]? = nil
    var synonyms: [String]? = nil
    var hazards: [String]? = nil
    var products: [String]? = nil
    var properties: [String]? = nil
    var identifiers: [String]? = nil
    
    init() {
        self.fireCodes = [""]
        self.nfpa = [""]
        self.name = [""]
        self.toxicology = [""]
        self.regulations = [""]
        self.conditions = [""]
        self.synonyms = [""]
        self.hazards = [""]
        self.products = [""]
        self.properties = [""]
        self.identifiers = [""]
    }
    init(fireCodes: [String], nfpa: [String], name: [String], toxicology: [String], regulations: [String], conditions: [String], synonyms: [String], hazards: [String], products: [String], properties: [String], identifiers: [String]) {
        self.fireCodes = fireCodes
        self.nfpa = nfpa
        self.name = name
        self.toxicology = toxicology
        self.regulations = regulations
        self.conditions = conditions
        self.synonyms = synonyms
        self.hazards = hazards
        self.products = products
        self.properties = properties
        self.identifiers = identifiers
    }
    
    init(data:JSON) {
        self.fireCodes = data["chemInfo", "Fire Codes"].arrayObject as! [String]?
        self.nfpa = data["chemInfo", "NFPA Classifications"].arrayObject as! [String]?
        self.name = data["chemInfo", "Chemical"].arrayObject as! [String]?
        self.toxicology = data["chemInfo", "Toxicology"].arrayObject as! [String]?
        self.regulations = data["chemInfo", "Regulations"].arrayObject as! [String]?
        self.conditions = data["chemInfo", "Storage Conditions"].arrayObject as! [String]?
        self.synonyms = data["chemInfo", "Synonyms"].arrayObject as! [String]?
        self.hazards = data["chemInfo", "Hazards"].arrayObject as! [String]?
        self.products = data["chemInfo", "Products"].arrayObject as! [String]?
        self.properties = data["chemInfo", "Physical Properties"].arrayObject as! [String]?
        self.identifiers = data["chemInfo", "Additional Identifiers"].arrayObject as! [String]?
        
    }
    
    func toString() -> String {
        let _chemName: String? =  "Chemical Name:\n" + self.name!.joinWithSeparator("\n")
        let _fireCodes: String? = "\nFire Codes:\n" + self.fireCodes!.joinWithSeparator("\n")
        let _nfpa: String? = "\nNFPA:\n" + self.nfpa!.joinWithSeparator("\n")
        let _toxicology: String? = "\nToxicology:\n" + self.toxicology!.joinWithSeparator("\n")
        let _regulations: String? = "\nRegulations:\n" + self.regulations!.joinWithSeparator("\n")
        let _conditions: String? = "\nConditions:\n" + self.conditions!.joinWithSeparator("\n")
        let _synonyms: String? = "\nSynonyms:\n" + self.synonyms!.joinWithSeparator("\n")
        let _hazards: String? = "\nHazards:\n" + self.hazards!.joinWithSeparator("\n")
        let _products: String? = "\nProducts:\n" + self.products!.joinWithSeparator("\n")
        let _identifiers: String? = "\nIdentifiers:\n" + self.identifiers!.joinWithSeparator("\n")
        let stringed: [String] = [_chemName!, _fireCodes!, _nfpa!, _toxicology!, _regulations!, _conditions!, _synonyms!, _hazards!, _products!, _identifiers!]
        let retString: String = stringed.joinWithSeparator("")
        return retString
        
    }
    
    func toOrderRequest(dateRequired: String = "", grantId: String = "",vendorName: String = "Sigma", reqId: String = "", poNum: String = "", unitSize: String = "each", unitPrice: String = "", notes: String = "From iOS App") -> [String:String] {
        let orderJson = [
            "dateRequired":dateRequired,
            "grantId":grantId,
            "itemType":"Chemical",
            "vendorName":vendorName,
            "catalogNumber":self.identifiers?.first,
            "reqId":reqId,
            "poNum":poNum,
            "itemName":self.name?.first,
            "url":self.products?.first,
            "unitSize":unitSize,
            "unitPrice":unitPrice,
            "notes":notes
        
        ]
        return orderJson as! [String:String]
        
        
    }
    
    
    
}