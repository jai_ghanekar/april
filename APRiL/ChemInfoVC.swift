//
//  ChemInfoVC.swift
//  APRiL
//
//  Created by Jai Ghanekar on 5/18/16.
//  Copyright © 2016 Jai Ghanekar. All rights reserved.
//

import UIKit

class ChemInfoVC: UIViewController {
    var chemical: Chemical
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var orderButton: UIButton!
    
     init() {
        self.chemical = Chemical()
        super.init(nibName: nil, bundle: nil)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        self.chemical = Chemical()
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        self.textView.text = self.chemical.toString()
        self.orderButton.backgroundColor = UTD_GREEN
        self.orderButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.orderButton.layer.cornerRadius = 5.0
        self.orderButton.layer.borderWidth = 1.0
        
    }
    override func viewDidAppear(animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = self.chemical.name?.first
    }
    
    @IBAction func orderChemical(sender: AnyObject) {
        let orderRequestVC: OrderRequestVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("OrderRequest") as! OrderRequestVC
        orderRequestVC.chemical = chemical
        self.navigationController?.pushViewController(orderRequestVC, animated:true)
        
    }
    

}
