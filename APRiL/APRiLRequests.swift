//
//  APRiLRequests.swift
//  APRiL
//
//  Created by Jai Ghanekar on 5/17/16.
//  Copyright © 2016 Jai Ghanekar. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class APRiLRequests {
    class func getChemInfoWithId(barcode: String, onSuccess: (chemical: Chemical) -> (), onFailure:(response: Response<AnyObject, NSError>) -> ()){
        Alamofire.request(.GET, String(format: "http://%@:%@/%@/%@", HOST, PORT, API_VERSION, INFO), parameters: ["id":barcode])
         .responseJSON { response in
            if let httpError = response.result.error {
                let status = httpError.code
                if status == 500 {
                    print("Internal Server Error \(status)")
                    onFailure(response: response)
                }
            }
            else {
                if let data = response.result.value {
                    let jsonData = JSON(data)
                    let foundChemical = Chemical(data: jsonData)
                    onSuccess(chemical: foundChemical)
                    
                } else {
                    onFailure(response: response)
                }
                
            }
        
        }
    }
    
    class func createOrderRequest(params:[String: AnyObject]?, onSuccess: () -> (), onFailure:(response: Response<AnyObject, NSError>) -> ()) {
        Alamofire.request(.POST, String(format: "http://%@:%@/%@/%@", HOST, PORT, API_VERSION, ORDER), parameters: params, encoding:.JSON)
        .responseJSON { response in
            if let httpError = response.result.error {
                let status = httpError.code
                if status == 500 {
                    print("Internal Server Error \(status)")
                    onFailure(response: response)
                }
            }
            else {
                onSuccess()
            }
        
        }
        
    }
    
  
    
}