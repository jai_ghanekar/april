//
//  constants.swift
//  APRiL
//
//  Created by Jai Ghanekar on 5/17/16.
//  Copyright © 2016 Jai Ghanekar. All rights reserved.
//

import Foundation
import UIKit

let HOST:String = "localhost"
let PORT:String = "5000"
let API_VERSION:String = "api/v1"
let INFO:String = "info"
let ORDER:String = "orders"
let UTD_GREEN: UIColor = UIColor(red: 5.0/255, green: 144.0/255, blue: 72.0/255, alpha: 1)