//
//  OrderRequestVC.swift
//  APRiL
//
//  Created by Jai Ghanekar on 5/20/16.
//  Copyright © 2016 Jai Ghanekar. All rights reserved.
//

import UIKit

class OrderRequestVC: UIViewController {
    var chemical: Chemical
    @IBOutlet weak var dateReqField: UITextField!
    @IBOutlet weak var grandIdField: UITextField!
    @IBOutlet weak var vendorNameField: UITextField!
    @IBOutlet weak var reqIdField: UITextField!
    @IBOutlet weak var poNumberField: UITextField!
    @IBOutlet weak var unitSizeField: UITextField!
    @IBOutlet weak var unitPriceField: UITextField!
    @IBOutlet weak var notesField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBAction func submit(sender: AnyObject) {
        APRiLRequests.createOrderRequest(self.chemical.toOrderRequest(self.dateReqField.text!, grantId: self.grandIdField.text!, vendorName: self.vendorNameField.text!, reqId: self.reqIdField.text!, poNum: self.poNumberField.text!, unitSize: self.unitSizeField.text!, unitPrice: self.unitPriceField.text!, notes: self.notesField.text!), onSuccess: {
            print("order request created")
        }) { (response) in
            print("Failed to create order request")
        }
    }
    
    init() {
        self.chemical = Chemical()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.chemical = Chemical()
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        self.submitButton.backgroundColor = UTD_GREEN
        self.submitButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.submitButton.layer.cornerRadius = 5.0
        self.submitButton.layer.borderWidth = 1.0
    }
    
    
    

}
